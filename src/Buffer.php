<?php

	namespace Itul\Buffer;

	//THIS CLASS WILL BUFFER OUTPUT AND REUTURN IT AS A STRING
	class Buffer {

		//ASSUME THE BUFFER HASNT BEEN FLUSHED
		private static $_flushed = false;

		//BUFFER OUTPUT WITHOUT RENDERING AND RETURN THE RESULT AS A STRING OR A FALSE VALUE ON FAILURE
		public static function start($callback, $vars = []){
			
			//START BUFFER
			ob_start();

			//RUNN THE CALLBACK WITH THE VARS
			$callback($vars);

			//GET THE BUFFER AS A STRING
			$res = ob_get_clean();

			//RETURN THE RESULT IF IT EXISTS ELSE RETURN FALSE
			return strlen($res) ? $res : false;			
		}

		//TRY TO FLUSH THE BUFFER TO THE SCREEN
		public static function flush(){

			//FORCE THE CONTENT HEADER
			if(!headers_sent()) header("Content-Encoding: none");;

			//CHECK IF THE BUFFER HAS BEEN FLUSHED YET
			if(!self::$_flushed){

				//SET BUFFER OUTPUT RENDERING OPTIONS
				@ini_set('zlib.output_compression', 'Off');
				@ini_set('output_buffering', 'Off');
				if(function_exists('apache_setenv')) @apache_setenv('no-gzip', 1);
				if(!headers_sent()) header('X-Accel-Buffering: no');
				self::$_flushed = true;
			}

			//TRY TO FINISH FLUSHING THE BUFFER TO THE BROWSER
			@ob_end_flush();
			ob_implicit_flush(1);
		}
	}